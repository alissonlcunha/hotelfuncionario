# Requisitos do Sistema de Hotelaria #

Um hotel possui uma quantidade de quartos pré-determinada no momento do cadastro deste no sistema.
Perante o sistema todo quarto de hotel será armazenado respeitando determinadas características. O primeiro
aspecto é a quantidade de camas que há no quarto. Essa configuração dependerá da qualidade a ser ofertada,
mas pode ser 1 cama de casal, 2 camas de solteiro ou 1 cama de casal e 1 cama de solteiro. Caso seja necessário
um berço para crianças abaixo de 3 anos é possível solicitá-lo a qualquer momento após a solicitação de reserva
do quarto estar concluída.

Um quarto possui um estado o qual este se encontra em um determinado momento de sua existência perante
o sistema. O primeiro estado é o de **Disponível**, significando que nenhum hóspede está ocupando o quarto nesse
momento. O segundo estado é o de **Ocupado**, significando que algum hóspede fez check in. O terceiro estado é o
de **Indisponível**, o que significa que o quarto está impossibilitado de ser realizado check in devido a alguma
reforma, dedetização ou outro serviço necessário. O último estado é o de **Fechado**, indicando que aquele quarto
não poderá mais ser utilizado para check in no hotel. Ao acrescentar um quarto ao hotel o estado inicial do quarto
será o estado **Disponível**.

Existem diversos serviços de quarto, como alimentação, higiene e reparos. Cada serviço de quarto deverá
possuir ao menos um funcionário responsável pelo serviço e cada serviço utilizará de recursos que deverão estar
descritos em conjunto com os serviços realizados.

Um funcionário deverá possuir as informações de nome, data de nascimento, endereço residencial válido,
carteira de trabalho e conta-salário. O funcionário não pode possuir menos de 18 anos. Além disso, o funcionário 
deve possuir cargos correspondentes com seu serviço, por exemplo, se ele trabalhar com a parte de manutenção
elétrica deverá possuir um cargo de eletricista. É possível que um funcionário tenha mais de 1 cargo ativo ao
mesmo tempo.

Um endereço residencial válido consiste nas seguintes informações: Tipo Logradouro, nome do logradouro,
número, complemento, bairro, cidade, estado, país, CEP.

Um funcionário deverá possuir uma data de contratação e informações utilizadas para uma futura demissão,
ou seja, data de demissão e o motivo desta demissão. É possível que um funcionário já tenha trabalhado no hotel,
assim é necessário um histórico dessas passagens do funcionário.

O serviço de camareira deverá ser realizado uma vez ao dia. No dia do checkout o serviço deverá ser realizado
no ato da saída, pois assim o quarto já ficará arrumado para que este possa estar apto para um novo check-in.

O sistema deverá controlar os produtos utilizados pelos serviços prestados pelos funcionários. O sistema
deverá alertar ao setor de estoque quais produtos deverão ser adquiridos devido ao seu estoque baixo. O sistema
deverá gerenciar produtos reutilizáveis, como berços, pois estes vão para as estadias, mas normalmente, voltam
para ser utilizados.