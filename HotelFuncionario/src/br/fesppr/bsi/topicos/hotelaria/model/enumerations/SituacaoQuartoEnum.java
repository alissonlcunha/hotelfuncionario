package br.fesppr.bsi.topicos.hotelaria.model.enumerations;

public enum SituacaoQuartoEnum {
	
    DISPONIVEL(1L, "Disponivel"),
    OCUPADO(2L, "Ocupado"),
    FECHADO(3L, "Fechado"),
	INDISPONIVEL(4L, "Indisponivel");

    private Long id;
    private String nome;

    private SituacaoQuartoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}