package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Servico {
	
	private Long id;
	private String descricao;
	private Date data;
	private Double valor;
	
	private List<Produto> produtos = new ArrayList<>();
	private List<TipoServico> tipoServicos = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<TipoServico> getTipoServicos() {
		return this.tipoServicos;
	}
	
	public void setTipoServicos(List<TipoServico> tipoServicos) {
		this.tipoServicos = tipoServicos;
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void realizarServico(Integer nrQuarto) {
		
	}

	
}