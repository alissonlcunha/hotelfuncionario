package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Gasto {
	
	private String tipo;
	private Date data;
	private float valor;
	
	private List<Estadia> estadias = new ArrayList<>();
	private List<Reserva> reservas = new ArrayList<>();
	private List<Hospede> hospedes = new ArrayList<>();
	
	public Gasto(String tipo, Date data, float valor) {		
		this.tipo = tipo;
		this.data = data;
		this.valor = valor;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Date getData() {
		return this.data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public float getValor() {
		return this.valor;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}
	
	public List<Estadia> getEstadias() {
		return this.estadias;
	}
	
	public void setEstadias(List<Estadia> estadias) {
		this.estadias = estadias;
	}
	
	public List<Reserva> getReservas() {
		return this.reservas;
	}
	
	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}
	
	public List<Hospede> getHospedes() {
		return this.hospedes;
	}
	
	public void setHospedes(List<Hospede> hospedes) {
		this.hospedes = hospedes;
	}	

}
