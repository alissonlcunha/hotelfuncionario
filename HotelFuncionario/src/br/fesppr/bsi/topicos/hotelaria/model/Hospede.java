package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.List;

public class Hospede {
	
	private Long id;
	private String nome;
	private Integer documento;
	private String telefone;	
	private Endereco endereco;
	private TipoHospede tipoHospede;
	
	private List<Reserva> reservas = new ArrayList<>();
	private List<Gasto> gastos = new ArrayList<>();
	
	public Hospede(String nome, Integer documento, String telefone, Endereco endereco) {
		super();
		this.nome = nome;
		this.documento = documento;
		this.telefone = telefone;
		this.setEndereco(endereco);
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getDocumento() {
		return this.documento;
	}

	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public List<Reserva> getQuartos() {
		return reservas;
	}
	
	public List<Gasto> getGastos() {
		return gastos;
	}

	public void setGastos(List<Gasto> gastos) {
		this.gastos = gastos;
	}

	public TipoHospede getTipoHospede() {
		return tipoHospede;
	}

	public void setTipoHospede(TipoHospede tipoHospede) {
		this.tipoHospede = tipoHospede;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void realizarCadastro() {
		
	}
	
	public void defineTipo() {
		
	}
	
}
