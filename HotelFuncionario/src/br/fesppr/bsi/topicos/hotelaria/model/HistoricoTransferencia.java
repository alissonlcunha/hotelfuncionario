package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.Date;

public class HistoricoTransferencia {
	
	private Date dataInicio;
	private Date dataFim;
	private String localidade;

	public HistoricoTransferencia(Date dataInicio, Date dataFim, String localidade) {		
		this.setDataInicio(dataInicio);
		this.setDataFim(dataFim);
		this.setLocalidade(localidade);	
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}	

}
