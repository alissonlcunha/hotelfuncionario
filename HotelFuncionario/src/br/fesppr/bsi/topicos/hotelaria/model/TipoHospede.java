package br.fesppr.bsi.topicos.hotelaria.model;

public class TipoHospede {
	
	private Boolean titular;
	private Boolean dependente;
	private Boolean privilegios;
	
	public TipoHospede(boolean titular, boolean dependente, boolean privilegios) {
		super();
		this.setTitular(titular);
		this.setDependente(dependente);
		this.setPrivilegios(privilegios);	
	}

	public Boolean getTitular() {
		return this.titular;
	}

	public void setTitular(Boolean titular) {
		this.titular = titular;
	}

	public Boolean getDependente() {
		return this.dependente;
	}

	public void setDependente(Boolean dependente) {
		this.dependente = dependente;
	}

	public Boolean getPrivilegios() {
		return this.privilegios;
	}

	public void setPrivilegios(Boolean privilegios) {
		this.privilegios = privilegios;
	}
	
}