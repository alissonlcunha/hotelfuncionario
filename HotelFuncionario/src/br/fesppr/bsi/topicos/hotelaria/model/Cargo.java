package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.List;

public class Cargo {

	private Long id;
	private String descricao;
	
	private List<TipoServico> tipoServicos = new ArrayList<>();
	
	public Cargo(String descricao, TipoServico tipo) {
		super();
		this.descricao = descricao;
		this.tipoServicos.add(tipo);

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<TipoServico> getTipoServicos() {
		return tipoServicos;
	}

	public void setTipoServicos(List<TipoServico> tipoServicos) {
		this.tipoServicos = tipoServicos;
	}
	
	public void possuiTipoServico(TipoServico servico) {
		
		}
	
}
