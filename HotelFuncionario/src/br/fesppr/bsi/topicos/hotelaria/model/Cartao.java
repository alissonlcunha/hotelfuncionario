package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.Date;

public class Cartao extends Pagamento {

	public Cartao(long idTransacao, String status, String moeda) {
		super(idTransacao, status, moeda);

	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public Date getDataValidade() {
		return dataValidade;
	}

	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}

	private int numero;

	private String bandeira;

	private String nomeTitular;

	private Date dataValidade;

}
