package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Funcionario {

	private Long id;
	private String nome;
	private Date dataNascimento;
	
	private Endereco endereco;
	private List<Contrato> contratos = new ArrayList<>();
	private List<Cargo> cargos = new ArrayList<>();
	private List<HistoricoTransferencia> transferencias = new ArrayList<>();
	private List<Servico> servicos = new ArrayList<>();
	private List<Hotel> hoteis = new ArrayList<>();
	
	public Funcionario() {
		super();
	}

	public Funcionario(String nome, Date dataNascimento, Endereco endereco, Contrato contrato, Hotel hotel, Cargo cargo) {
		super();
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
		this.contratos.add(contrato);
		this.hoteis.add(hotel);	
		this.cargos.add(cargo);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}

	public List<Cargo> getCargos() {
		return cargos;
	}

	public void setCargos(List<Cargo> cargos) {
		this.cargos = cargos;
	}

	public List<HistoricoTransferencia> getTransferencias() {
		return transferencias;
	}

	public void setTransferencias(List<HistoricoTransferencia> transferencias) {
		this.transferencias = transferencias;
	}

	public List<Servico> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servico> servicos) {
		this.servicos = servicos;
	}
	
}
