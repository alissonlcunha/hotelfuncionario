package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.Date;

public class Contrato {

	private Long id;
	private Date dataContratacao;
	private Date dataDemissao;
	private String motivoDemissao;
	private Integer carteiraTrabalho;
	private Integer contaSalario;
	
	public Contrato(Date dataContratacao, String historico, Integer carteiraTrabalho, Integer contaSalario) {		
		this.dataContratacao = dataContratacao;
		this.carteiraTrabalho = carteiraTrabalho;	
		this.contaSalario = contaSalario;	
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataContratacao() {
		return this.dataContratacao;
	}

	public void setDataContratacao(Date dataContratacao) {
		this.dataContratacao = dataContratacao;
	}

	public Date getDataDemissao() {
		return this.dataDemissao;
	}

	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public String getMotivoDemissao() {
		return this.motivoDemissao;
	}

	public void setMotivoDemissao(String motivoDemissao) {
		this.motivoDemissao = motivoDemissao;
	}

	public Integer getCarteiraTrabalho() {
		return this.carteiraTrabalho;
	}

	public void setCarteiraTrabalho(Integer carteiraTrabalho) {
		this.carteiraTrabalho = carteiraTrabalho;
	}

	public Integer getContaSalario() {
		return this.contaSalario;
	}

	public void setContaSalario(Integer contaSalario) {
		this.contaSalario = contaSalario;
	}	
	
	public void rescindirContrato(Long id) {
		this.setMotivoDemissao(motivoDemissao);
		this.setDataDemissao(dataDemissao);	
	}
}
