package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.List;

import br.fesppr.bsi.topicos.hotelaria.model.enumerations.SituacaoQuartoEnum;

public class Quarto {

	private Integer nrQuarto;
	private Integer andar;
	private Hotel hotel;
	
	private Categoria categoria;	
	private List<Produto> produtos = new ArrayList<>();	
	private SituacaoQuartoEnum situacaoQuarto;
	
	public Quarto() {
		super();
	}

	public Quarto(int nrQuarto, int andar, Categoria categoria, List<Produto> produtos, Hotel hotel) {
		super();
		this.nrQuarto = nrQuarto;
		this.andar = andar;
		this.categoria = categoria;
		this.produtos = produtos;
		this.situacaoQuarto = SituacaoQuartoEnum.DISPONIVEL;
		this.setHotel(hotel);
	}

	public int getNrQuarto() {
		return nrQuarto;
	}

	public void setNrQuarto(int nrQuarto) {
		this.nrQuarto = nrQuarto;
	}

	public int getAndar() {
		return andar;
	}

	public void setAndar(int andar) {
		this.andar = andar;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public SituacaoQuartoEnum getSituacaoQuarto() {
		return situacaoQuarto;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
		
	public void disponibilizarQuarto() {
		this.situacaoQuarto = SituacaoQuartoEnum.DISPONIVEL;
	}
	
	public void ocuparQuarto() {
		this.situacaoQuarto = SituacaoQuartoEnum.OCUPADO;
	}	
	
	public void indisponibilizarQuarto() {
		this.situacaoQuarto = SituacaoQuartoEnum.INDISPONIVEL;
	}
	
	public void fecharQuarto() {
		this.situacaoQuarto = SituacaoQuartoEnum.FECHADO;
	}
}