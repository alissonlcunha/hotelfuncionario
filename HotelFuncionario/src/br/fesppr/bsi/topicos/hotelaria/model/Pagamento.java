package br.fesppr.bsi.topicos.hotelaria.model;

public class Pagamento {

	private long idTransacao;

	private String status;

	private String moeda;
	
	public Pagamento(long idTransacao, String status, String moeda) {
		super();
		this.idTransacao = idTransacao;
		this.status = status;
		this.moeda = moeda;
	}

	public long getIdTransacao() {
		return idTransacao;
	}

	public void setIdTransacao(long idTransacao) {
		this.idTransacao = idTransacao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMoeda() {
		return moeda;
	}

	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}
	
	public void efetuarPagamento() {

	}

	public void reembolsoReserva() {

	}

	public void converteValor() {

	}


}