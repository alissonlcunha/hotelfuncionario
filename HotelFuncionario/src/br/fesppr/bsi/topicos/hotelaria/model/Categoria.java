package br.fesppr.bsi.topicos.hotelaria.model;

public class Categoria {

	private Long id;
	private String nome;
	private int quantidade;
	private Double valor;
	
	public Categoria(String nome, int quantidade, Double valor) {		
		this.nome = nome;
		this.quantidade = quantidade;
		this.valor = valor;	
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}	
	
}