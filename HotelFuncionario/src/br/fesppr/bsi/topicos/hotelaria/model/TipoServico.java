package br.fesppr.bsi.topicos.hotelaria.model;

public class TipoServico {
	
	private Long id;
	private String descricao;
	private Boolean diario;
	
	public TipoServico(String descricao, Boolean diario) {		
		this.descricao = descricao;
		this.diario = diario;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricaoServico() {
		return this.descricao;
	}

	public void setDescricaoServico(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getDiario() {
		return this.diario;
	}

	public void setDiario(Boolean diario) {
		this.diario = diario;
	}
	
}