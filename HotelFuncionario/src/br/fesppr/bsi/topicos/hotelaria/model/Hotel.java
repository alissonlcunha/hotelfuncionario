package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.List;

public class Hotel {
	private Long id;
	private String nome;	
	
	private Endereco endereco;
	private List<Funcionario> funcionarios = new ArrayList<>();
	private List<Reserva> reservas = new ArrayList<>();
	private List<Quarto> quartos = new ArrayList<>();
	
	
	public Hotel() {
		super();
	}

	public Hotel(String nome, Endereco endereco, List<Funcionario> funcionarios, List<Reserva> reservas,
			List<Quarto> quartos) {
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.funcionarios = funcionarios;
		this.reservas = reservas;
		this.quartos = quartos;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public List<Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}

	public List<Quarto> getQuartos() {
		return quartos;
	}

	public void setQuartos(List<Quarto> quartos) {
		this.quartos = quartos;
	}
	
	
	
	
	
}