package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Estadia {
	
	private Long id;
	private Date dataCheckin;
	private Date dataCheckout;	
	private List<Quarto> quartos = new ArrayList<>();
	private List<Pagamento> pagamentos = new ArrayList<>();
	private List<Gasto> gastos = new ArrayList<>();
	private List<Servico> servicos = new ArrayList<>();
	
	public Estadia() {
		super();
	}

	public Estadia(Quarto quarto, Pagamento pagamento) {
		super();
		this.quartos.add(quarto);
		this.pagamentos.add(pagamento);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Quarto> getQuartos() {
		return quartos;
	}

	public void setQuartos(List<Quarto> quartos) {
		this.quartos = quartos;
	}

	public Date getDataCheckin() {
		return dataCheckin;
	}

	public void setDataCheckin(Date dataCheckin) {
		this.dataCheckin = dataCheckin;
	}

	public Date getDataCheckout() {
		return dataCheckout;
	}

	public void setDataCheckout(Date dataCheckout) {
		this.dataCheckout = dataCheckout;
	}
	

	public List<Pagamento> getPagamentos() {
		return pagamentos;
	}

	public void setPagamentos(List<Pagamento> pagamentos) {
		this.pagamentos = pagamentos;
	}
	
	public List<Gasto> getGastos() {
		return gastos;
	}

	public void setGastos(List<Gasto> gastos) {
		this.gastos = gastos;
	}
	
	public List<Servico> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servico> servicos) {
		this.servicos = servicos;
	}
	
}
