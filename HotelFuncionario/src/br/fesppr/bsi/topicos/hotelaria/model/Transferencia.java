package br.fesppr.bsi.topicos.hotelaria.model;

public class Transferencia extends Pagamento {

	public Transferencia(long idTransacao, String status, String moeda) {
		super(idTransacao, status, moeda);

	}

	private int contaOrigem;

	private int contaDestino;

	private int cpfOrigem;

	private int cnpjDestino;

	public int getContaOrigem() {
		return contaOrigem;
	}

	public void setContaOrigem(int contaOrigem) {
		this.contaOrigem = contaOrigem;
	}

	public int getContaDestino() {
		return contaDestino;
	}

	public void setContaDestino(int contaDestino) {
		this.contaDestino = contaDestino;
	}

	public int getCpfOrigem() {
		return cpfOrigem;
	}

	public void setCpfOrigem(int cpfOrigem) {
		this.cpfOrigem = cpfOrigem;
	}

	public int getCnpjDestino() {
		return cnpjDestino;
	}

	public void setCnpjDestino(int cnpjDestino) {
		this.cnpjDestino = cnpjDestino;
	}

}
