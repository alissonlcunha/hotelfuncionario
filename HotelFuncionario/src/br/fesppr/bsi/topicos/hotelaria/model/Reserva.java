package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Reserva {
	
	private Long id;
	private Boolean berco;
	private Date dataCheckIn;
	private Date dataCheckOut;
	
	private Estadia estadia;
	private Hotel hotel;
	private Categoria categoria;
	
	private List<Gasto> gastos = new ArrayList<>();
	private List<Hospede> hospedes = new ArrayList<>();
	private List<Pagamento> pagamentos = new ArrayList<>();
	
	
	public Reserva() {
		super();
	}

	public Reserva(boolean berco, Date dataCheckIn, Date dataCheckOut, Estadia estadia,
				   Hotel hotel, Categoria categoria, Pagamento pagamento) {
		super();
		this.berco = berco;
		this.dataCheckIn = dataCheckIn;
		this.dataCheckOut = dataCheckOut;
		this.setCategoria(categoria);
		this.estadia = estadia;
		this.setHotel(hotel);
		this.pagamentos.add(pagamento);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isBerco() {
		return berco;
	}

	public void setBerco(boolean berco) {
		this.berco = berco;
	}

	public Date getDataCheckIn() {
		return dataCheckIn;
	}

	public void setDataCheckIn(Date dataCheckIn) {
		this.dataCheckIn = dataCheckIn;
	}

	public Date getDataCheckOut() {
		return dataCheckOut;
	}

	public void setDataCheckOut(Date dataCheckOut) {
		this.dataCheckOut = dataCheckOut;
	}
	public Estadia getEstadia() {
		return estadia;
	}

	public void setEstadia(Estadia estadia) {
		this.estadia = estadia;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Gasto> getGastos() {
		return gastos;
	}

	public void setGastos(List<Gasto> gastos) {
		this.gastos = gastos;
	}

	public List<Hospede> getHospedes() {
		return hospedes;
	}

	public void setHospedes(List<Hospede> hospedes) {
		this.hospedes = hospedes;
	}	

	public void realizarReserva() {		
		
	}
	
	public void cancelarReserva() {		
		
	}
	
	public void selecionarCategoria(Categoria categoria) {
		
	}
	
	public void realizarCheckin(Pagamento pagamento) {
		
	}
	
	public void realizarCheckout(Pagamento pagamento) {
		
	}
	
	public void disparaEmail() {
		
	}
		
	public void solicitaAdiantamento(Pagamento pagamaento) {
		
	}
	
}