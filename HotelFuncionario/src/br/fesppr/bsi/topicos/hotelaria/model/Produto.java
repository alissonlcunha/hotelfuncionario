package br.fesppr.bsi.topicos.hotelaria.model;

import java.util.Date;

public class Produto {
	
	private Long id;
	private String descricao;
	private Integer quantidade;
	private Date prazoValidade;
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getProduto() {
		return this.descricao;
	}
	
	public void setProduto(String descricao) {
		this.descricao = descricao;
	}
	
	public Integer getQuantidade() {
		return this.quantidade;
	}
	
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Date getPrazoValidade() {
		return prazoValidade;
	}

	public void setPrazoValidade(Date prazoValidade) {
		this.prazoValidade = prazoValidade;
	}
	
	public void checarEstoque() {
		
	}
}